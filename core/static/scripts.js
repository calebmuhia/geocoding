'use strict'


angular.module("geocoding", [
     'ngSanitize',
    
    'ngResource',
    'ngRoute',
    'ngCookies',
    
    'ui.bootstrap',
    'cgBusy',
    ])

.controller('GeoCodingctrl', ['$scope','$http', function($scope, $http){

    $scope.submit_coordinates = function(){

        $scope.coordinates_promise = $http.get("/get_address/?data="+$scope.coordinates )
        .then(function(res){
            $scope.coordinates_results = res.data

        })
    }

     $scope.submit_address = function(){

        $scope.address_promise = $http.get("/get_coordinates/?data="+$scope.address)
        .then(function(res){
            $scope.address_results = res.data

        })
    }
    
    
}])