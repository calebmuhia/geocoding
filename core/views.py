from django.shortcuts import render, render_to_response
from django.views.generic import TemplateView
from django.template.context import RequestContext
import re, json

from django.http import HttpResponse

from geopy.geocoders import Nominatim


# Create your views here.


class ExtraActionMixin(TemplateView):
    action = None

    def dispatch(self, request, *args, **kwargs):
        """
        Submitting form works only for "GET" and "POST".
        If `action` is defined use it dispatch request to the right method.
        """
        if not self.action:
            return super(ExtraActionMixin, self).dispatch(request, *args,
                **kwargs)
        if self.action in self.http_method_names:
            handler = getattr(self, self.action, self.http_method_not_allowed)
        else:
            handler = getattr(self, self.action, None)
        self.request = request
        self.args = args
        self.kwargs = kwargs
        return handler(request, *args, **kwargs)


class Home(ExtraActionMixin):

    template_name = "core/home.html"

    def get(self, request, *args, **kwargs):

        ctx = self.get_context_data(**kwargs)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        
        return render_to_response(self.template_name, ctx)

    def get_address(self, request, *args, **kwargs):
        coordinates = self.request.GET.get("data", None)

        coords = re.split(' |, |,',coordinates) 

        geolocator = Nominatim(timeout=10)

        str_format = "{0}, {1}".format(str(coords[0]), str(coords[1]))
        

        location = geolocator.reverse(str_format)

        

        return HttpResponse(location.address) 


    def get_coordinates(self, request, *args, **kwargs):
        address = self.request.GET.get("data", None)
        geolocator = Nominatim(timeout=10)
        location = geolocator.geocode(address)

        return HttpResponse("{0}, {1}".format(location.latitude, location.longitude))        





