from django.conf.urls import include, url
from core.views import Home


urlpatterns = [
    url(r'^$', Home.as_view(), name="home" ),
    url(r'^get_address/', Home.as_view(action="get_address"), name="get_address" ),
    url(r'^get_coordinates/', Home.as_view(action="get_coordinates"), name="get_coordinates" ),



]
